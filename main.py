from hardwares.camera import is_violating, VideoStream
from hardwares.proximity_sensor import ProximitySensor
from hardwares.servo import Servo

import RPi.GPIO as GPIO
import signal
import time
import threading
import websockets
import base64
import asyncio
import cv2
import json

GENDER = "male"

class Websocket(threading.Thread):
    def __init__(self, loop, stream):
        self.loop = loop
        self.stream = stream
        self.__connected_clients__ = []

        threading.Thread.__init__(self)

    def run(self):
        while True:
            if self.stream.get_binary() is not None:
                msg = {
                    'type': 'image',
                    'data': base64.b64encode(self.stream.get_binary()).decode('utf-8')
                }

                self.broadcast(json.dumps(msg))

    async def handler(self, websocket, path):
        self.__connected_clients__.append(websocket)
        try:
            await websocket.recv()
        except websockets.exceptions.ConnectionClosed:
            pass
        finally:
            self.__connected_clients__.remove(websocket)

    def __send_data__(self, client, data):
        cl = client.send(data)
        asyncio.run_coroutine_threadsafe(cl, self.loop)

    def broadcast(self, data):
        for client in self.__connected_clients__:
            subworker = threading.Thread(target=self.__send_data__, args=(client, data))
            subworker.start()
        time.sleep(0.1)

def graceful_exit(a, b):
    GPIO.cleanup()
    exit()

def handler(cap, ws):
    def func():
        _, frame = cap.read()
        _, binary = cv2.imencode('.jpg', frame)

        violating = is_violating(GENDER, binary.tobytes()) 
        msg = {
            'type': 'message',
            'data': "Mendeteksi.. {}".format("melanggar!" if violating else "tidak melanggar.")
        }

        ws.broadcast(json.dumps(msg))
        if violating:
            servo.set_angle(90)
            time.sleep(5)
            servo.set_angle(15)
        
    return func

### RASPI SETUP ###

signal.signal(signal.SIGTERM, graceful_exit)
signal.signal(signal.SIGINT, graceful_exit)
GPIO.setmode(GPIO.BOARD)

### HARDWARE SETUP ###

proximity_sensor = ProximitySensor(18, 12)
servo = Servo(7)
cap = cv2.VideoCapture(0)
video = VideoStream(cap)

### WEBSOCKET SETUP ###

loop = asyncio.get_event_loop()
worker = Websocket(loop, video)
server = websockets.serve(worker.handler, '0.0.0.0', 8765)

watcher = threading.Thread(target=proximity_sensor.watch, args=(200, handler(cap, worker)))

### ASYNC AND THREADS STARTUP ###

video.start()
watcher.start()
worker.start()
loop.run_until_complete(server)
loop.run_forever()
