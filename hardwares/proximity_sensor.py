import RPi.GPIO as GPIO
import time

class ProximitySensor:
    def __init__(self, echo_pin, trigger_pin):
        GPIO.setup(echo_pin, GPIO.IN)
        GPIO.setup(trigger_pin, GPIO.OUT)

        self.echo_pin = echo_pin
        self.trigger_pin = trigger_pin
    
    def __get_distance__(self):
        GPIO.output(self.trigger_pin, True)
        time.sleep(0.00001)
        GPIO.output(self.trigger_pin, False)

        start = time.time()
        stop = time.time()

        while GPIO.input(self.echo_pin) == 0:
            start = time.time()
        
        while GPIO.input(self.echo_pin) == 1:
            stop = time.time()

        return (stop - start) * 34300 / 2
    
    def watch(self, trigger_distance, callback):
        while True:
            distance = self.__get_distance__()
            print(distance)

            if distance <= trigger_distance:
                callback()
            
            time.sleep(0.01)
