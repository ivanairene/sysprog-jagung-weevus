import RPi.GPIO as GPIO
import time

class Servo:
    def __init__(self, pin):
        GPIO.setup(pin, GPIO.OUT)

        self.pin = pin
        self.pwm = GPIO.PWM(7, 50)
        self.pwm.start(0)

    def set_angle(self, angle):
        duty = (angle / 18) + 2
        GPIO.output(self.pin, True)
        self.pwm.ChangeDutyCycle(duty)
        
        time.sleep(1)
        GPIO.output(self.pin, False)
        self.pwm.ChangeDutyCycle(0)
