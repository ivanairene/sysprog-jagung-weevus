import subprocess
import requests
import threading
import time
import cv2

IMG_FILENAME = 'img.jpg'
CMD = 'fswebcam -r 640x480 --jpeg 85 -d /dev/video0 -i 0 -D 1 -S 10 {}'.format(IMG_FILENAME)
IMAGGA_KEY = 'acc_08cefb13b0cb9c0'
IMAGGA_SECRET = 'e43ccf7254ce48cdb6bcca09b848f4d8'
TAGS = {
    "male": ['male', 'man', 'boy', 'handsome', 'men', 'guy', 'businessman'],
    "female": ['businesswoman', 'lady', 'women', 'pretty', 'feminime'],
}

class VideoStream(threading.Thread):
    def __init__(self, cap):
        self.binary = None
        self.cap = cap
        threading.Thread.__init__(self)

    def run(self):
        while True:
            _, frame = self.cap.read()
            _, self.binary = cv2.imencode('.jpg', frame)
            time.sleep(0.1)
    
    def get_binary(self):
        return self.binary

def is_violating(gender, data):
    tags_response = requests.post(
        'https://api.imagga.com/v2/tags',
        auth=(IMAGGA_KEY, IMAGGA_SECRET), 
        files={'image': data}
    ).json()['result']['tags']

    filtered_response = [tag for tag in tags_response if tag['tag']['en'] in TAGS["male"] + TAGS["female"]]
    result = get_gender_confidence("male", filtered_response)

    return result >= 51

def get_gender_confidence(gender, imagga_response):
    tags = TAGS[gender]

    confidence = 0
    for tag in imagga_response:
        if tag['tag']['en'] in tags:
            confidence += tag['confidence']
    
    return confidence

